## Terminologie MII KDS Basismodul Consent

Dieses Paket enthält die Terminologie aus dem Basismodul Consent des Kerndatensatzes der MII.

Diese Ressourcen wurden umpaketiert aus dem Ursprungspaket: https://simplifier.net/packages/de.medizininformatikinitiative.kerndatensatz.consent/2025.0.1.

---

Sobald neue Versionen veröffentlicht werden, werden diese auf anderen Git-Branches verfügbar gemacht. Sie sehen aktuell den Stand vom Branch **2025**.

Weitere Branches:

- [1.0.7](https://gitlab.com/mii-termserv/fhir-resources/mii-kerndatensatz/de.medizininformatikinitiative.kerndatensatz.terminology.consent/-/tree/1.0.7?ref_type=heads)
